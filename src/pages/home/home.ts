  import { Component } from '@angular/core';
  import { NavController, Platform ,AlertController} from 'ionic-angular';
  import { AdMobPro } from '@ionic-native/admob-pro';
import { TechtutorialPage } from '../techtutorial/techtutorial';
import { Observable } from 'rxjs/Rx';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  subscription;
  constructor(public navCtrl: NavController, public admob: AdMobPro, private platform: Platform) {
      this.createBanner(); 
  }

  ionViewDidLoad() {
    this.subscription = Observable.interval(1000).subscribe(x => {
      console.log(x)
      if(x == 20){
        this.showIntAdd()

      }
   

     
    
    });
    this.admob.onAdDismiss()
      .subscribe(() => { console.log('User dismissed ad'); });
     
  }
 


  createBanner() {
    this.platform.ready().then(()=>{
     let options = {
       adId : 'ca-app-pub-3001267101498085/4332504287',
       isTesting: false,
     };
       this.admob.createBanner(options).then(()=>{
         this.admob.showBanner(8);
       })
    });

  }
    
  pageNavigation(page){
    if(page == 'techtutorial'){
      this.navCtrl.push(TechtutorialPage)
    }
  }  

  showIntAdd() {
    let adId;
    if(this.platform.is('android')) {
      adId = 'ca-app-pub-3001267101498085/7880505668';
    } else if (this.platform.is('ios')) {
      adId = 'ca-app-pub-3001267101498085/7880505668';
    }
    this.admob.prepareInterstitial({adId: adId})
      .then(() => { this.admob.showInterstitial(); });
  }

  


}
