import { Component } from '@angular/core';
import { AdMobPro } from '@ionic-native/admob-pro';
import {NavController, NavParams,Platform } from 'ionic-angular';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import {Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Observable } from 'rxjs/Rx';
/**
 * Generated class for the TechtutorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-techtutorial',
  templateUrl: 'techtutorial.html',
})
export class TechtutorialPage {
  data: any;
  techTutorialArray : any[] = [];
  message:any  = null;
  subscription;
  file:any=null;
  link:any = null
  subject: any;
  constructor(public admob: AdMobPro, private platform: Platform,private socialSharing: SocialSharing,public http: Http,private youtube: YoutubeVideoPlayer,public navCtrl: NavController, public navParams: NavParams) {
    this.data = null;
    this.getData();
    this.techTutorialArray = []
   
      
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TechtutorialPage');
    this.subscription = Observable.interval(1000).subscribe(x => {
      console.log(x)
      if(x == 10){
        this.showIntAdd()

      }
      if(x == 200){
        this.showIntAdd()

      }
      if(x == 500){
        this.showIntAdd()

      }

    
    });
  }
  
  openFirstVideo(){
    this.youtube.openVideo('QX632bSf4N4&t=430s');
  }

  getData(){
    if(this.data){
      return Promise.resolve(this.data)
    }
    return new Promise(resolve =>{

       this.http.get('http://serverside.pythonanywhere.com/learnGuru/techtutorial')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
        console.log(data)
        for(let index=0; index<data.length;index++){
          this.techTutorialArray.push({
            video_title: data[index].video_title,
            video_id: data[index].video_id,
            image_url: data[index].image_url,
            video_duration: data[index].video_duration,
   
  
          })

          console.log(this.techTutorialArray)
        }
       
      })
      console.log(this.data);
    })

  }
  PlayVideo(videoId){
    this.youtube.openVideo(videoId);
  }

  share(videoId){
    this.message = "https://www.youtube.com/watch?v="+videoId

    this.socialSharing.share(this.message, this.subject, this.file, this.link).then(()=>{
    }).catch(()=>{

    });
  }
  showIntAdd() {
    console.log("CLICKED")
    let adId;
    if(this.platform.is('android')) {
      adId = 'ca-app-pub-3001267101498085/7880505668';
    } else if (this.platform.is('ios')) {
      adId = 'ca-app-pub-3001267101498085/7880505668';
    }
    this.admob.prepareInterstitial({adId: adId})
      .then(() => { this.admob.showInterstitial(); });
  }
}
