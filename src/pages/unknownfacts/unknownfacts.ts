import { Component } from '@angular/core';
import { NavController, NavParams,Platform } from 'ionic-angular';
import { ImageViewerController } from 'ionic-img-viewer';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import {Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Observable } from 'rxjs/Rx';

/**
 * Generated class for the UnknownfactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-unknownfacts',
  templateUrl: 'unknownfacts.html',
})
export class UnknownfactsPage {
  _imageViewerCtrl: ImageViewerController;
  value: string = "images";
  data: any;
  dataVideo:any;
  file:any=null;
  link:any = null
  subject: any;
  message:any  = null;
  unknownFactArray : any[] = [];
    constructor(imageViewerCtrl: ImageViewerController, private platform: Platform,private socialSharing: SocialSharing,public http: Http,private youtube: YoutubeVideoPlayer,public navCtrl: NavController, public navParams: NavParams) {
  this._imageViewerCtrl = imageViewerCtrl;
  this.data = null;
  
    this.getUnknownFactVideo()
    this.getUnknownFactImage()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UnknownfactsPage');
  }
  presentImage(myImage) {
    
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
 
    setTimeout(() => imageViewer.dismiss(), 10000);
    imageViewer.onDidDismiss(() => console.log('Viewer dismissed'));
  }

  getUnknownFactImage(){
    if(this.data){
      return Promise.resolve(this.data)
    }
    return new Promise(resolve =>{

       this.http.get('http://serverside.pythonanywhere.com/learnGuru/unknownfacts')
      .map(res => res.json())
      .subscribe(data => {
        this.data = data;
        resolve(this.data);
        console.log(data)
        for(let index=0; index<data.length;index++){
          this.unknownFactArray.push({
            image_title: data[index].video_title,
            image_subtitle  : data[index].video_id,
            image_url: data[index].image_url,
          })

        }
       
      })
      console.log(this.unknownFactArray);
    })
  }
  getUnknownFactVideo(){
    if(this.dataVideo){
      return Promise.resolve(this.dataVideo)
    }
    return new Promise(resolve =>{

       this.http.get('http://serverside.pythonanywhere.com/learnGuru/ufvideo')
      .map(res => res.json())
      .subscribe(data => {
        this.dataVideo = data;
        resolve(this.data);
        console.log(this.dataVideo)
      })
      console.log(this.dataVideo);
    })
  }
  PlayVideo(videoId){
    this.youtube.openVideo(videoId);
  }

  share(videoId){
    this.message = "https://www.youtube.com/watch?v="+videoId

    this.socialSharing.share(this.message, this.subject, this.file, this.link).then(()=>{
    }).catch(()=>{

    });
  }
  shareImage(imageUrl){
    this.message = 'LearnGuroo Unknow facts image ',
    this.socialSharing.share(this.message, this.subject, this.file, imageUrl).then(()=>{
    }).catch(()=>{

    });
  }


}
